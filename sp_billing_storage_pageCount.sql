  -- Executed to get account wise details
  
-- =============================================  
-- Author:      <Prithivitaj KS>  
-- Create Date: <06/12/2018>  
-- Description: <Total number of pages added in acount>  
-- =============================================  
CREATE       PROCEDURE [dbo].[sp_billing_storage_pageCount]  
(  
 @accountId VARCHAR(50),  
 @StartDate DATETIME2,  
 @Enddate DATETIME2  
)  
AS  
BEGIN  
  
 SET NOCOUNT ON;  
   
 DECLARE @IsUserBreakDown bit = 0;  
  
 DECLARE @ReportStartDate date = CAST(@StartDate as date)  
 DECLARE @ReportEndDate date = CAST(@EndDate as date)  
 DECLARE @PreviousDay date = DATEADD(DAY,-1,@ReportEndDate)  
 DECLARE @CurrentDay date = CAST(GETUTCDATE() as DATE)  
  
 DECLARE @Result as Table  
 (  
  AccountId  varchar(50),  
  PreviousPages   int,  
  AddedPages  int,  
  RemovedPages int  
 )  
  
 DECLARE @ExistingDailyReports as Table  
 (  
  AccountId  varchar(50),  
  PreviousPages   int,  
  AddedPages  int,  
  RemovedPages int  
 )  
  
 DECLARE @NewDailyReports as Table  
 (  
  AccountId  varchar(50),  
  PreviousPages   int,  
  AddedPages  int,  
  RemovedPages int  
 )  
   
 --No need to show current day's report  
 /*  
 IF(@ReportEndDate >= @CurrentDay)  
 BEGIN  
  --If report includes current date  
  INSERT INTO @NewDailyReports  
  EXEC [dbo].[sp_daily_event_storage_count] @accountId, @CurrentDay, @IsUserBreakDown  
   
  --Get data from DailyEventReport for other than current day  
  INSERT INTO @ExistingDailyReports  
  EXEC [dbo].[sp_get_storage_from_daily_event_report] @accountId, @ReportStartDate, @PreviousDay, @IsUserBreakDown  
  
  INSERT INTO @Result  
  SELECT  
   A.AccountId,  
   0 as PreviousPages,  
   SUM(A.AddedPages) as AddedPages,  
   SUM(A.RemovedPages) as RemovedPages  
  FROM  
  (  
   SELECT * FROM @NewDailyReports  
   UNION ALL  
   SELECT * FROM @ExistingDailyReports  
  ) A  
  GROUP BY A.AccountId  
  
  UPDATE R SET  
   R.PreviousPages = ND.PreviousPages  
  FROM @Result R  
  INNER JOIN @ExistingDailyReports ND  
  ON R.AccountId = ND.AccountId  
  
 END  
 ELSE  
 BEGIN*/  
  INSERT INTO @Result  
  EXEC [dbo].[sp_get_storage_from_daily_event_report] @accountId, @ReportStartDate, @ReportEndDate, @IsUserBreakDown  
 --END  
  
 SELECT   
  PreviousPages,  
  AddedPages,  
  RemovedPages  
 FROM @Result  
END  